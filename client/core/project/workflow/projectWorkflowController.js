angular
	.module('corail')
	.controller('projectWorkflowController', ($scope, $window, Status, Prototype, prototypes) => {
		const statuses = Status.get('prototype')

		let columns = {}
		for (let status in statuses) columns[status] = prototypes.filter(prototype => prototype.status === status)

		$scope.statuses		= statuses
		$scope.prototypes	= prototypes
		$scope.columns		= columns

		const over = event => {
			event.preventDefault()
			event.dataTransfer.dropEffect = 'move'
			return false
		}

		const enter = (event, element) => {
			event.preventDefault()
			element.classList.add('workflow__column--over')
		}

		const leave = (event, element) => {
			if (event.target === element) element.classList.remove('workflow__column--over')
		}

		const start = event => {
			let target	= event.target
			event.dataTransfer.setData('text', target.id)
			target.classList.add('dragged')
		}

		const drop = (event, element) => {
			event.preventDefault()
			event.stopPropagation()
			let data				= event.dataTransfer.getData('text')
			let targetStatus		= element.id
			let [, status, index]	= data.match(/^(.+)_(\d+)$/)
			let column				= columns[status]
			let prototype			= column[index]
			prototype.status = targetStatus
			Prototype.update(prototype, () => {
				element.classList.remove('workflow__column--over')
				columns[targetStatus].push(prototype)
				column.splice(index, 1)
			}, err => {
				console.error(err)
			})
		}

		const end = event => {
			event.target.classList.remove('dragged')
		}

		$window.over	= over
		$window.enter	= enter
		$window.leave	= leave
		$window.start	= start
		$window.drop	= drop
		$window.end		= end
	})
