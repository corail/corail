angular
	.module('corail')
	.controller('projectsController', ($scope, Status, Project, projects) => {
		$scope.statuses			= Status.get('project')
		$scope.cardStatusClass	= Status.getCardStatusClass('project')
		$scope.projects			= projects

		let statusesCount = {}
		for (let project of projects) {
			let status = project.status
			if (!statusesCount[status]) statusesCount[status] = 1
			else statusesCount[status]++
		}

		$scope.statusesCount		= statusesCount
		$scope.statusesCountLength	= Object.keys(statusesCount).length - 1
	})
