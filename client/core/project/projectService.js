angular
	.module('corail')
	.factory('Project', $resource => {
		return $resource('/api/projects/:id/:action', { id: '@id' }, {
			update: {
				method: 'PUT',
				params: { id: null }
			},
			uploadImage: {
				method: 'POST',
				params: { action: 'image' },
				transformRequest: x => x,
				headers: { 'Content-Type': undefined }
			}
		})
	})
