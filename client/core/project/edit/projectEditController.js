angular
	.module('corail')
	.controller('projectEditController', ($scope, $state, $stateParams, Project, project) => {
		let startedAt	= project.startedAt
		let endedAt		= project.endedAt
		if (startedAt) project.startedAt = new Date(startedAt)
		if (endedAt) project.endedAt = new Date(endedAt)

		$scope.project = project

		const editProject = () => {
			Project.update(project, result => {
				Object.assign(project, result)
				$state.go('^')
			}, err => {
				console.log(err)
			})
		}

		$scope.uploadImage	= Project.uploadImage
		$scope.editProject	= editProject
	})
