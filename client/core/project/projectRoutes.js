angular
	.module('corail')
	.config($stateProvider => {
		$stateProvider
			.state('projects', {
				url:			'/projects',
				resolve:		{ projects: Project => Project.query().$promise },
				controller:		'projectsController',
				templateUrl:	'core/project/projects.html',
			})

			.state('projectsNew', {
				parent: 		'projects',
				url:			'/new',
				isModal:		true,
				controller:		'projectAddController',
				templateUrl:	'core/project/edit/edit.html'
			})

			.state('project', {
				abstract:		true,
				url:			'/project/:id',
				resolve: {
					project: (Project, $stateParams) => Project.get($stateParams).$promise,
					prototypes: (Prototype, $stateParams) => Prototype.query($stateParams).$promise
				},
				controller:		'projectController',
				templateUrl:	'core/project/project.html'
			})
			.state('project.view', {
				url:			'',
				controller:		'projectViewController',
				templateUrl:	'core/project/view/view.html'
			})
			.state('project.workflow', {
				url:			'/workflow',
				controller:		'projectWorkflowController',
				templateUrl:	'core/project/workflow/workflow.html'
			})

			.state('projectEdit', {
				parent:			'project.view',
				url:			'/edit',
				isModal: 		true,
				controller:		'projectEditController',
				templateUrl:	'core/project/edit/edit.html'
			})
	})
