angular
	.module('corail')
	.controller('projectViewController', ($scope, Status, project, prototypes) => {
		$scope.project			= project
		$scope.statuses			= Status.get('prototype')
		$scope.projectStatuses	= Status.get('project')
		$scope.cardStatusClass	= Status.getCardStatusClass('prototype')
		$scope.prototypes		= prototypes
	})
