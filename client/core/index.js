angular
	.module('corail', [
		'ngResource',
		'ngSanitize',
		'angular.filter',
		'angular-loading-bar',
		'color.picker',
		'ui.router'
	])
	.config(($urlRouterProvider, $locationProvider, $stateProvider) => {
		// Base route URL
		$urlRouterProvider.otherwise('/projects')

		// Commented until we have a server
		// $locationProvider.html5Mode(true)

		// Handle modal's state
		let modalView		= { template: '<div ui-view="modal"></div>' }
		let modalViewParent	= {
			controller:		'modalController',
			templateUrl:	'components/modal/modal.html'
		}

		$stateProvider.decorator('views', (state, parent) => {
			if (state.views) return parent(state)
			let views	= state.views = {}
			let view	= {
				templateUrl:	state.templateUrl,
				controller:		state.controller
			}
			if (state.isModal) views.modal = view
			else {
				views.modal		= !state.parent.parent ? modalViewParent : modalView
				views.content	= view
			}
			return parent(state)
		})
	})
	.run(($rootScope, $anchorScroll) => {
		let modal		= {}
		let application	= { modal }

		$rootScope.$on('$stateChangeSuccess', (event, target) => {
			let isModal = target.isModal
			modal.open		= isModal
			modal.closing	= false
			if (!isModal) $anchorScroll()
		})

		$rootScope.application = application
	})
