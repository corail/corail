angular
	.module('corail')
	.config($stateProvider => {
		$stateProvider
			.state('prototype', {
				abstract:		true,
				url:			'/prototype/:id',
				templateUrl:	'core/prototype/prototype.html',
				resolve: {
					prototype: (Prototype, $stateParams) => Prototype.get($stateParams).$promise,
					project: (Project, prototype) => Project.get({id: prototype.projectId }).$promise
				},
				controller: 	'prototypeController'
			})

			.state('prototype.view', {
				url:			'?fullscreen',
				params: 		{ expanded: true },
				templateUrl:	'core/prototype/view/view.html',
				controller:		'prototypeViewController'
			})
			.state('prototype.version', {
				url:			'/versions',
				templateUrl:	'core/prototype/version/version.html',
				controller:		'prototypeVersionController',
				resolve:		{
					versions: (Version, $stateParams) => Version.query($stateParams).$promise
				}
			})
			.state('prototype.draw', {
				url:			'/draw',
				templateUrl:	'core/prototype/draw/draw.html',
				controller:		'prototypeDrawController'
			})
			.state('prototype.comment', {
				url:			'/comments',
				templateUrl:	'core/prototype/comment/comment.html',
				controller:		'prototypeCommentController',
				resolve: {
					comments: (Comment, $stateParams) => Comment.query($stateParams).$promise
				}
			})

			.state('prototypeNew', {
				parent: 		'project.view',
				url: 			'/prototypes/new',
				isModal: 		true,
				templateUrl: 	'core/prototype/edit/edit.html',
				controller: 	'prototypeAddController'
			})
			.state('prototypeEdit', {
				parent: 		'prototype.view',
				url: 			'/edit',
				isModal: 		true,
				templateUrl: 	'core/prototype/edit/edit.html',
				controller: 	'prototypeEditController'
			})
			.state('prototypeVersionAdd', {
				parent: 		'prototype.draw',
				url: 			'/add',
				isModal: 		true,
				templateUrl: 	'core/prototype/version/add.html',
				controller: 	'versionAddController'
			})
			.state('prototypeCommentAdd', {
				parent: 		'prototype.comment',
				url:			'/add/:x/:y',
				isModal:		true,
				templateUrl:	'core/prototype/comment/add.html',
				controller:		'commentAddController'
			})
			.state('prototypeVersionEdit', {
				parent: 		'prototype.draw',
				url:			'/edit',
				isModal:		true,
				templateUrl:	'core/prototype/version/edit.html',
				controller:		'versionEditController'
			})
			.state('prototypeVersionExport', {
				parent: 		'prototype.draw',
				url:			'/export',
				isModal:		true,
				templateUrl:	'core/prototype/export/export.html',
				controller:		'exportController'
			})
			.state('prototypeDrawIcon', {
				parent: 		'prototype.draw',
				url:			'/icon',
				params:			{ target: null, listeners: null },
				isModal:		true,
				templateUrl:	'core/prototype/draw/icon.html',
				controller:		'iconController'
			})
	})
