angular
	.module('corail')
	.factory('Version', $resource => {
		return $resource('/api/versions/:version/:id', { version: null, id: '@id'}, {
			get: {
				method:'GET',
				params: { version: 'version', id: '@id'}
			},
			update: {
				method: 'PUT',
				params: { id: null }
			}
		})
	})
