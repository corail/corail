angular
	.module('corail')
	.controller('prototypeVersionController', ($scope, $document, prototype, versions) => {
		let active				= versions.length - 1
		let contentContainer	= $document[0].getElementById('content')

		$scope.versions	= versions
		$scope.active	= active
		$scope.version	= versions[active]

		const select = index => {
			let version = versions[index]
			if (!version) return
			let content = version.content
			$scope.active	= index
			$scope.version	= version
			if (content) contentContainer.innerHTML = content
		}

		$scope.select = select

		if (versions.length) select(active)
	})
