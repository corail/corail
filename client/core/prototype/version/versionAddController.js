angular
	.module('corail')
	.controller('versionAddController', ($scope, $state, Version, prototype) => {
		let prototypeId	= prototype.id
		let version

		version = angular.copy(prototype.version)
		version.prototypeId = prototypeId
		if (!version.tag) version.tag = '1.0.0'
		if (version.id) delete version.id

		$scope.version = version

		const addVersion = () => {
			version.userId = 1
			Version.save(version, result => {
				Object.assign(version, result)
				prototype.version = version
				$state.go('prototype.version')
			}, err => {
				console.log(err)
			})
		}

		$scope.addVersion = addVersion
	})
