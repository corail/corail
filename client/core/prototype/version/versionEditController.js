angular
	.module('corail')
	.controller('versionEditController', ($scope, $state, Version, prototype) => {
		let version = prototype.version

		version.prototypeId = prototype.id
		$scope.version = version

		const editVersion = () => {
			Version.update(version, result => {
				Object.assign(version, result)
				$state.go('prototype.version')
			}, err => {
				console.log(err)
			})
		}

		$scope.editVersion = editVersion
	})
