angular
	.module('corail')
	.factory('Comment', $resource => {
		return $resource('/api/comments/:id', { id: '@id' }, {
			update: {
				method: 'PUT',
				params: { id: null }
			}
		})
	})
