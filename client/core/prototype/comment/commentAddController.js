angular
	.module('corail')
	.controller('commentAddController', ($scope, $state, $stateParams, Comment, comments, prototype) => {
		let comment		= {}
		let positionX	= $stateParams.x
		let positionY	= $stateParams.y
		let prototypeId = prototype.id

		$scope.comment = comment

		const addComment = () => {
			comment.userId		= 1
			comment.prototypeId	= prototypeId
			comment.positionX	= positionX
			comment.positionY	= positionY

			Comment.save(comment, result => {
				$scope.comment = comment = {}
				comments.push(result)
				$state.go('^')
			}, err => {
				console.log(err)
			})
		}

		$scope.addComment = addComment
	})
