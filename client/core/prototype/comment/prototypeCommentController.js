angular
	.module('corail')
	.controller('prototypeCommentController', ($scope, $document, $state, Comment, Response, comments, prototype) => {
		let responses							= []
		let document							= $document[0]
		let content								= prototype.version.content
		let contentContainer					= document.getElementById('content')
		let { top: offsetY, left: offsetX }		= contentContainer.getBoundingClientRect()

		if (content) contentContainer.innerHTML = content

		$scope.showResolved	= false
		$scope.states		= {}
		$scope.responses	= responses
		$scope.comments		= comments

		const addComment = event => {
			let x = Math.round(event.clientX * 1.25 - offsetX)
			let y = Math.round(event.clientY * 1.25 - offsetY)
			$state.go('prototypeCommentAdd', { x, y })
		}

		const toggleProcessed = index => {
			let commentOriginal	= comments[index]
			let comment			= angular.copy(commentOriginal)
			commentOriginal.isProcessed	= !commentOriginal.isProcessed
			commentOriginal.isDisabled	= true
			Comment.update(comment, result => {
				commentOriginal.isDisabled	= false
				commentOriginal.isProcessed	= result.isProcessed
			}, err => {
				console.log(err)
			})
		}

		const addResponse = index => {
			let comment	= comments[index]
			Response.save({
				commentId:	comment.id,
				userId:		1,
				content:	responses[index]
			}, result => {
				responses[index] = ''
				if (comment.responses) comment.responses.push(result)
				else comment.responses = [result]
			}, err => {
				console.log(err)
			})
		}

		$scope.addComment 		= addComment
		$scope.toggleProcessed 	= toggleProcessed
		$scope.addResponse 		= addResponse
	})
