angular
	.module('corail')
	.factory('Response', $resource => {
		return $resource('/api/responses', null, null)
	})
