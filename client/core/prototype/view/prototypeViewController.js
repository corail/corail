angular
	.module('corail')
	.controller('prototypeViewController', ($scope, $document, $stateParams, Status) => {
		let content	= $scope.version.content

		if (content) $document[0].getElementById('content').innerHTML = content

		$scope.statuses		= Status.get('prototype')
		$scope.expanded		= $stateParams.expanded
		$scope.fullscreen	= $stateParams.fullscreen
	})
