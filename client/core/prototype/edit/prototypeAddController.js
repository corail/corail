angular
	.module('corail')
	.controller('prototypeAddController', ($scope, $state, Status, Prototype, project, prototypes) => {
		let prototype = {}

		$scope.statuses 	= Status.get('prototype')
		$scope.prototype 	= prototype

		const editPrototype = () => {
			prototype.projectId = project.id
			Prototype.save(prototype, result => {
				prototypes.push(result)
				$state.go('^')
			}, err => {
				console.log(err)
			})
		}

		$scope.editPrototype = editPrototype
	})
