angular
	.module('corail')
	.controller('prototypeEditController', ($scope, $state, Status, Prototype, prototype) => {
		$scope.prototype 	= prototype
		$scope.statuses 	= Status.get('prototype')

		const editPrototype = () => {
			Prototype.update(prototype, () => $state.go('^'), err => {
				console.error(err)
			})
		}

		$scope.editPrototype = editPrototype
	})
