angular
	.module('corail')
	.controller('prototypeController', ($scope, prototype, project) => {
		$scope.project		= project
		$scope.prototype	= prototype
		$scope.version		= prototype.version = prototype.version || {}
	})
