angular
	.module('corail')
	.factory('Prototype', $resource => {
		return $resource('/api/prototypes/:prototype/:id', { prototype: null, id: '@id'}, {
			get: {
				method:'GET',
				params: { prototype: 'prototype', id: '@id'}
			},
			update: {
				method: 'PUT',
				params: { id: null }
			}
		})
	})