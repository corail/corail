angular
	.module('corail')
	.controller('prototypeDrawController', ($scope, $state, $document, prototype, fontFamilies, DOM) => {
		let document	= $document[0]
		let version		= $scope.version
		let content		= document.getElementById('content')
		let container	= document.getElementById('container')
		let toggle		= {}
		let selected

		$scope.fontFamilies	= fontFamilies
		$scope.expanded		= {
			content:	true,
			layout:		true
		}

		const { units, properties } = DOM

		if (version.content) {
			content.innerHTML = version.content
			for (let element of content.querySelectorAll('.wrapper')) {
				element.addEventListener('click', elementClick)
				element.addEventListener('mouseover', mouseover)
				element.addEventListener('mouseout', mouseout)
			}
		}

		$scope.toggle = toggle

		const toggleBar = (direction, event) => {
			event.preventDefault()
			event.stopPropagation()
			$scope.toggle[direction] = !$scope.toggle[direction]
		}

		const save = () => {
			let versionContent = version.content
			version.content = content.innerHTML
			$state.go(versionContent ? 'prototypeVersionEdit': 'prototypeVersionAdd')
		}

		const setSize = element => {
			let size = element.getBoundingClientRect()
			element.setAttribute('width', round(size.width))
			element.setAttribute('height', round(size.height))
			return element
		}

		const addElement = (parent, child) => {
			let wrapper = document.createElement('div')
			wrapper.className = 'wrapper'
			wrapper.addEventListener('click', elementClick)
			wrapper.addEventListener('mouseover', mouseover)
			wrapper.addEventListener('mouseout', mouseout)
			parent.appendChild(wrapper)
			if (child) {
				wrapper.appendChild(child)
				setSize(wrapper)
			}
			return wrapper
		}

		const addMarkup = (type, ...classes) => {
			let markup = document.createElement(type)
			toggle.bottom = false
			markup.classList.add('element', ...classes)
			let wrapper = addElement(selected || content, markup)
			if (classes.includes('inline')) wrapper.classList.add('inline')
		}

		const addColumns = columnSize => {
			let wrapper = addElement(selected || content)
			toggle.bottom = false
			for (let i = 12 / columnSize; i--; ) {
				let subElement = document.createElement('div')
				subElement.classList.add('element', 'block')
				let element = addElement(wrapper, subElement)
				element.classList.add(`large-${columnSize}`, 'columns')
			}
			let clearElement = document.createElement('div')
			clearElement.classList.add('clear')
			wrapper.appendChild(clearElement)
			setSize(wrapper)
		}

		const addToList = (type) => {
			let element = document.createElement(type)
			element.classList.add('item')
			selected.appendChild(element)
		}

		const removeFromList = (index) => {
			safeRemove(selected.children[index], true)
		}

		const safeRemove = (element, forced) => {
			if (!forced && (!element.classList.contains('wrapper') || element.hasChildNodes())) return
			let parent = element.parentElement
			parent.removeChild(element)
			safeRemove(parent)
		}

		const remove = () => {
			if (!selected) return
			let parent		= selected.parentElement
			let next		= parent.nextSibling
			let offset		= next && /large-(\d)+/.exec(parent.className)
			if (offset) {
				let currentOffset	= /large-offset-(\d)+/.exec(parent.className)
				let nextOffset		= /large-offset-(\d)+/.exec(next.className)
				offset = parseInt(offset[1])
				if (currentOffset) offset += parseInt(currentOffset[1])
				if (nextOffset) {
					nextOffset = parseInt(nextOffset[1])
					offset += nextOffset
					next.classList.remove(`large-offset-${nextOffset}`)
					next.classList.add(`large-offset-${offset}`)
				} else next.classList.add('large-offset-' + offset)
				safeRemove(parent, true)
			} else safeRemove(selected, true)
			$scope.selected = selected = undefined
			resetBar()
		}

		const resetBar = () => {
			resetSelected()
			$scope.toggle = toggle = {}
		}

		const resetSelected = element => {
			if (selected) {
				selected.parentElement.classList.remove('selected')
				if (!element) return $scope.selected = selected = undefined
			}
			element = element && element.firstChild
			if (!element) return
			if (!element.units) element.units = angular.copy(units)
			let style = getComputedStyle(element)
			for (let property of properties) element.style[property] = style[property]
			if (element.style.lineHeight === 'normal') element.style.lineHeight = 1
			if (element.style.letterSpacing === 'normal') element.style.letterSpacing = 0
			if (element.style.textAlign === 'start') element.style.textAlign = 'left'
			if (element.style.boxShadow === 'none') {
				element.style.boxShadow = '0 0 0 0 rgb(0,0,0)'
				element.boxShadow = { y: '0px', blur: '0px', spread: '0px', color: 'rgb(0, 0, 0)' }
			}
			if (element.classList.contains('block')) {
				element.style.width = style.width
				element.style.height = style.height
			}
			element.parentElement.classList.add('selected')
			$scope.selected = selected = element
		}

		const changeProperty = (name, value, reset) => {
			let style = selected.style
			style[name] = style[name] === value ? reset : value
		}

		const getIcon = () => $state.go('prototypeDrawIcon', {
			target: selected || content,
			listeners: { mouseover, mouseout, click: elementClick }
		})

		const getImage = () => {
			let parent	= selected || content
			let image	= document.createElement('img')
			toggle.bottom = false
			image.classList.add('image')
			let size = parent.getBoundingClientRect()
			image.src = `http://placehold.it/${round(size.width)}x${round(size.height)}`
			addElement(parent, image)
		}

		$scope.toggleBar		= toggleBar
		$scope.remove			= remove
		$scope.addColumns		= addColumns
		$scope.addMarkup		= addMarkup
		$scope.addToList		= addToList
		$scope.removeFromList	= removeFromList
		$scope.changeProperty	= changeProperty
		$scope.getIcon			= getIcon
		$scope.getImage			= getImage

		$scope.$watch('selected.style.width', () => {
			if (!selected) return
			selected.parentElement.setAttribute('width', round(selected.getBoundingClientRect().width))
		})

		$scope.$watch('selected.style.height', () => {
			if (!selected) return
			selected.parentElement.setAttribute('height', round(selected.getBoundingClientRect().height))
		})

		container.addEventListener('click', () => {
			resetBar()
			$scope.$apply()
		})
		document.addEventListener('keyup', event => {
			event.preventDefault()
			event.stopPropagation()
			switch (event.keyCode) {
				case 27:
					resetBar()
					break
				case 45:
					if (!event.altKey) return
					toggle.bottom = true
					break
				case 46:
					if (!event.altKey) return
					remove()
					resetBar()
					break
				case 83:
					if (event.altKey) save()
					return
				default: return //console.log(event.keyCode)
			}
			$scope.$apply()
		})

		function elementClick (event) {
			event.preventDefault()
			event.stopPropagation()
			resetSelected(this)
			if (event.altKey) toggle.right = true
			else toggle.left = true
			$scope.$apply()
		}

		function mouseover (event) {
			event.preventDefault()
			event.stopPropagation()
			this.classList.add('hovered')
		}

		function mouseout (event) {
			event.preventDefault()
			event.stopPropagation()
			this.classList.remove('hovered')
		}

		function round (value) {
			return Math.round(value * 100) / 100
		}
	})
