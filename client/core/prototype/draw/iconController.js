angular
	.module('corail')
	.controller('iconController', ($scope, $state, $stateParams, $document, icons) => {
		let document	= $document[0]
		let listeners	= $stateParams.listeners
		let target		= $stateParams.target

		$scope.icons = icons

		const choose = (name, event) => {
			event.preventDefault()
			event.stopPropagation()
			let icon	= document.createElement('i')
			let wrapper	= document.createElement('span')
			icon.classList.add('inline', 'text', 'fa', `fa-${name}`)
			wrapper.appendChild(icon)
			wrapper.classList.add('wrapper', 'inline')
			wrapper.addEventListener('click', listeners.click)
			wrapper.addEventListener('mouseover', listeners.mouseover)
			wrapper.addEventListener('mouseout', listeners.mouseout)
			target.appendChild(wrapper)
			let size = wrapper.getBoundingClientRect()
			wrapper.setAttribute('width', Math.round(size.width * 100) / 100)
			wrapper.setAttribute('width', Math.round(size.height * 100) / 100)
			$state.go('^')
		}

		$scope.choose = choose
	})
