angular.module('corail').directive('imagePicker', () => {
	let imagePicker, imagePreview

	let controller = $scope => {
		let choose = () => {
			imagePicker.click()
		}

		let remove = () => {
			imagePreview.empty()
			$scope.data.image = null
		}

		$scope.choose = choose
		$scope.remove = remove
	}

	let link = (scope, element) => {
		imagePicker		= element.find('#imagePicker')
		imagePreview	= element.find('#imagePreview')

		imagePicker.bind('change', event => {
			let image = event.target.files[0]
			if (!image) return
			let data	= scope.data
			let reader	= new FileReader()
			reader.readAsDataURL(image)
			reader.onload = event => {
				let imageElement	= imagePreview.empty()[0].appendChild(document.createElement('img'))
				let formData		= new FormData()
				imageElement.className = 'thumbnail'
				imageElement.src = event.target.result
				scope.uploading = true
				formData.append('file', image)
				scope.upload(data, formData, result => {
					scope.uploading = false
					Object.assign(data, result)
				}, err => {
					console.error(err)
					scope.remove()
					scope.uploading = false
				})
			}
			scope.image = image
			scope.$apply()
		})
	}

	return {
		restrict: 'E',
		scope: {
			data: '=',
			upload: '=',
			params: '='
		},
		templateUrl: 'components/directives/imagePicker/imagePicker.html',
		link,
		controller
	}
})
