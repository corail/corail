angular
	.module('corail')
	.directive('ngModelType', () => {
		return {
			restrict: 'A',
			require: '^ngModel',
			scope: { ngModelType: '=' },
			link (scope, element, attributes, ngModelController) {
				ngModelController.$parsers.push(x => x)
				if (scope.ngModelType = 'number') {
					ngModelController.$formatters.push(value => Number(value))
				}
			}
		}
	})
