angular.module('corail').directive('hello', () => {
	return {
		restrict: 'E',
		scope: {
			name: '@'
		},
		templateUrl: 'components/directives/hello/hello.html'
	}
})
