angular
	.module('corail')
	.directive('ngModelSuffix', () => {
		const getSuffix = value => (Array.isArray(value) ? value : [value]).join(' ')
		const getValue = (value, suffixes) => {
			suffixes = Array.isArray(suffixes) ? suffixes : [suffixes]
			for (let suffix of suffixes) value = value.replace(suffix, '')
			return value.replace(/ */g, '')
		}

		return {
			restrict: 'A',
			require: '^ngModel',
			scope: { ngModelSuffix: '=' },
			link (scope, element, attributes, ngModelController) {
				let watcher = scope.$watch('ngModelSuffix', () => {
					scope.$watch('ngModelSuffix', suffixes => {
						if (!suffixes || !Array.isArray(suffixes)) return
						let value = ngModelController.$viewValue
						ngModelController.$setViewValue(null)
						ngModelController.$setViewValue(value)
					}, true)
				}, true)
				ngModelController.$parsers.push(value => value + getSuffix(scope.ngModelSuffix))
				ngModelController.$formatters.push(value => Number(value && getValue(value, scope.ngModelSuffix)))
			}
		}
	})
