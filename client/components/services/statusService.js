angular
	.module('corail')
	.factory('Status', Constants => {
		const prefixClass	= 'card__container--'
		const types			= Constants.status

		let statusesClass = {}

		const get = type => types[type]

		const getCardStatusClass = type => cardStatus => {
			let result = Object.assign({}, get(type))
			result[prefixClass + cardStatus] = true
			return result
		}

		for (let type in types) {
			let statusClass = statusesClass[type] = {}
			for (let status of Object.keys(get(type))) statusClass[prefixClass + status] = false
		}

		return {
			get,
			getCardStatusClass
		}
	})
