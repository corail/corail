angular
	.module('corail')
	.filter('prettyDate', () => {
		let weekDays 	= ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
		let months 		= ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

		return input => {
			let date 	= new Date(input)
			let now 	= new Date()
			let diff 	= now - date

			let oneMin 	= 60000
			let oneHour = 60 * oneMin
			let oneDay 	= 24 * oneHour
			let day 	= date.getDay()

			if (diff < 2 * oneMin) return 'à l\'instant'
			if (diff < oneHour) return 'il y a quelques minutes'
			let nowDay = now.getDay()
			if (nowDay === day && diff < oneDay) return 'aujourd\'hui'
			if (nowDay - 1 === day && diff < 2 * oneDay) return 'hier'
			if (diff < 7 * oneDay) return weekDays[day] + ' dernier'
			return `le ${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`
		}
	})
