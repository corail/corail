angular
	.module('corail')
	.filter('initials', () => {
		return input => input.split(' ').map(value => value.charAt(0)).join('')
	})
