angular
	.module('corail')
	.filter('dateFormat', () => {
		return input => {
			if (!input) return '?'

			let date	= new Date(input)
			let day 	= '0' + date.getDate()
			let month 	= `0${date.getMonth() + 1}`

			return `${day.slice(-2)}/${month.slice(-2)}/${date.getFullYear()}`
		}
	})
