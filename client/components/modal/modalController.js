angular
	.module('corail')
	.controller('modalController', ($rootScope, $scope, $timeout, $state) => {
		let modal = $rootScope.application.modal

		const close = () => {
			if (modal.closing) return
			modal.closing = true
			$timeout(() => $state.go('^'), 200)
		}

		$scope.close = close
	})
