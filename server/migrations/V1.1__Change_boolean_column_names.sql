-- Model: Corail
-- Version: 1.1

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `corail`.`comment`
-- -----------------------------------------------------
ALTER TABLE `corail`.`comment`
	CHANGE `processed` `isProcessed` TINYINT(1) NOT NULL DEFAULT 0,
	CHANGE `private` `isPrivate` TINYINT(1) NOT NULL DEFAULT 0;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
