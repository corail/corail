-- Model: Corail
-- Version: 1.0

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `corail`.`role`
-- -----------------------------------------------------
CREATE TABLE `corail`.`role` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(45) NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`user`
-- -----------------------------------------------------
CREATE TABLE `corail`.`user` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`role_id` INT,
	`firstname` VARCHAR(45) NULL,
	`lastname` VARCHAR(45) NULL,
	`email` VARCHAR(45) NOT NULL,
	`password` VARCHAR(20) NOT NULL,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	PRIMARY KEY (`id`),
	CONSTRAINT `user_role`
		FOREIGN KEY (`role_id`)
		REFERENCES `corail`.`role` (`id`)
		ON DELETE SET NULL
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`project`
-- -----------------------------------------------------
CREATE TABLE `corail`.`project` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(45) NULL,
	`type` VARCHAR(45) NULL,
	`content` VARCHAR(250) NULL,
	`image` VARCHAR(45) NULL,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	`updated_at` DATETIME NOT NULL DEFAULT NOW(),
	`started_at` DATETIME NULL,
	`ended_at` DATETIME NULL,
	PRIMARY KEY (`id`)
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`prototype`
-- -----------------------------------------------------
CREATE TABLE `corail`.`prototype` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`project_id` INT NOT NULL,
	`name` VARCHAR(45) NULL,
	`content` VARCHAR(250) NULL,
	`status` ENUM('waiting', 'in_progress', 'revision', 'validated') NOT NULL DEFAULT 'waiting',
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	`updated_at` DATETIME NOT NULL DEFAULT NOW(),
	PRIMARY KEY (`id`, `project_id`),
	CONSTRAINT `prototype_project`
		FOREIGN KEY (`project_id`)
		REFERENCES `corail`.`project` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`version`
-- -----------------------------------------------------
CREATE TABLE `corail`.`version` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`prototype_id` INT NOT NULL,
	`user_id` INT,
	`name` VARCHAR(250) NULL,
	`content` TEXT NOT NULL,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	PRIMARY KEY (`id`, `prototype_id`),
	CONSTRAINT `version_prototype`
		FOREIGN KEY (`prototype_id`)
		REFERENCES `corail`.`prototype` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION,
	CONSTRAINT `version_user`
		FOREIGN KEY (`user_id`)
		REFERENCES `corail`.`user` (`id`)
		ON DELETE SET NULL
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`document`
-- -----------------------------------------------------
CREATE TABLE `corail`.`document` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`content` TEXT NOT NULL,
	`prototype_id` INT NOT NULL,
	PRIMARY KEY (`id`, `prototype_id`),
	CONSTRAINT `document_prototype`
		FOREIGN KEY (`prototype_id`)
		REFERENCES `corail`.`prototype` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`comment`
-- -----------------------------------------------------
CREATE TABLE `corail`.`comment` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`prototype_id` INT NOT NULL,
	`user_id` INT,
	`content` TEXT NOT NULL,
	`processed` TINYINT(1) NOT NULL DEFAULT 0,
	`private` TINYINT(1) NOT NULL DEFAULT 0,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	`updated_at` DATETIME NOT NULL DEFAULT NOW(),
	`ended_at` DATETIME NULL,
	PRIMARY KEY (`id`, `prototype_id`),
	CONSTRAINT `comment_prototype`
		FOREIGN KEY (`prototype_id`)
		REFERENCES `corail`.`prototype` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION,
	CONSTRAINT `comment_user`
		FOREIGN KEY (`user_id`)
		REFERENCES `corail`.`user` (`id`)
		ON DELETE SET NULL
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`response`
-- -----------------------------------------------------
CREATE TABLE `corail`.`response` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`comment_id` INT NOT NULL,
	`user_id` INT,
	`content` TEXT NOT NULL,
	`created_at` DATETIME NOT NULL DEFAULT NOW(),
	PRIMARY KEY (`id`, `comment_id`),
	CONSTRAINT `response_comment`
		FOREIGN KEY (`comment_id`)
		REFERENCES `corail`.`comment` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION,
	CONSTRAINT `response_user`
		FOREIGN KEY (`user_id`)
		REFERENCES `corail`.`user` (`id`)
		ON DELETE SET NULL
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `corail`.`user_project`
-- -----------------------------------------------------
CREATE TABLE `corail`.`user_project` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`project_id` INT NOT NULL,
	PRIMARY KEY (`id`, `user_id`, `project_id`),
	CONSTRAINT `project_user`
		FOREIGN KEY (`project_id`)
		REFERENCES `corail`.`project` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION,
	CONSTRAINT `user_project`
		FOREIGN KEY (`user_id`)
		REFERENCES `corail`.`user` (`id`)
		ON DELETE CASCADE
		ON UPDATE NO ACTION
)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
