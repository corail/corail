-- Model: Corail
-- Version: 1.2

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';



-- -----------------------------------------------------
-- Table `corail`.`user`
-- -----------------------------------------------------
ALTER TABLE `corail`.`user`
	CHANGE `role_id` `roleId` INT,
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW();


-- -----------------------------------------------------
-- Table `corail`.`project`
-- -----------------------------------------------------
ALTER TABLE `corail`.`project`
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW(),
	CHANGE `updated_at` `updatedAt` DATETIME NOT NULL DEFAULT NOW(),
	CHANGE `started_at` `startedAt` DATETIME NULL,
	CHANGE `ended_at` `endedAt` DATETIME NULL;


-- -----------------------------------------------------
-- Table `corail`.`prototype`
-- -----------------------------------------------------
ALTER TABLE `corail`.`prototype`
	CHANGE `project_id` `projectId` INT NOT NULL,
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW(),
	CHANGE `updated_at` `updatedAt` DATETIME NOT NULL DEFAULT NOW();


-- -----------------------------------------------------
-- Table `corail`.`version`
-- -----------------------------------------------------
ALTER TABLE `corail`.`version`
	CHANGE `prototype_id` `prototypeId` INT NOT NULL,
	CHANGE `user_id` `userId` INT,
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW();


-- -----------------------------------------------------
-- Table `corail`.`document`
-- -----------------------------------------------------
ALTER TABLE `corail`.`document`
	CHANGE `prototype_id` `prototypeId` INT NOT NULL;


-- -----------------------------------------------------
-- Table `corail`.`comment`
-- -----------------------------------------------------
ALTER TABLE `corail`.`comment`
	CHANGE `prototype_id` `prototypeId` INT NOT NULL,
	CHANGE `user_id` `userId` INT,
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW(),
	CHANGE `updated_at` `updatedAt` DATETIME NOT NULL DEFAULT NOW(),
	CHANGE `ended_at` `endedAt` DATETIME NULL;


-- -----------------------------------------------------
-- Table `corail`.`response`
-- -----------------------------------------------------
ALTER TABLE `corail`.`response`
	CHANGE `comment_id` `commentId` INT NOT NULL,
	CHANGE `user_id` `userId` INT,
	CHANGE `created_at` `createdAt` DATETIME NOT NULL DEFAULT NOW();


-- -----------------------------------------------------
-- Table `corail`.`user_project`
-- -----------------------------------------------------
ALTER TABLE `corail`.`user_project`
	CHANGE `user_id` `userId` INT NOT NULL,
	CHANGE `project_id` `projectId` INT NOT NULL;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
