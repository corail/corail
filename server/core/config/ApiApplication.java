package config;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.mustache.MustacheMvcFeature;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("api")
class ApiApplication extends ResourceConfig {
    public ApiApplication () {
        // Register resources and providers using package-scanning.
        packages("api", "config.provider");

		// JSON Support
		register(MoxyJsonFeature.class);

		// File uploading support
		register(MultiPartFeature.class);

		// Mustache templates support
		property(MustacheMvcFeature.TEMPLATE_BASE_PATH, "/templates");
		register(MustacheMvcFeature.class);

		// Logs
		register(LoggingFeature.class);
    }
}
