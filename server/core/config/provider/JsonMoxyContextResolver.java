package config.provider;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.moxy.json.MoxyJsonConfig;

@Provider
public class JsonMoxyContextResolver implements ContextResolver<MoxyJsonConfig> {
	@Override
	public MoxyJsonConfig getContext (Class<?> objectType) {
		final Map<String, String> namespacePrefixMapper = new HashMap<>();
		namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");

		return new MoxyJsonConfig()
			.setNamespacePrefixMapper(namespacePrefixMapper)
			.setNamespaceSeparator(':')
			.setMarshalEmptyCollections(false);
	}
}
