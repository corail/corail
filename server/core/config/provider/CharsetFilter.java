package config.provider;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Provider
public class CharsetFilter implements ContainerRequestFilter, ContainerResponseFilter {
	@Override
	public void filter (ContainerRequestContext request) {
		String contentType = getContentType(request.getMediaType());
		if (contentType != null) request.getHeaders().putSingle("Content-Type", contentType);
	}

	@Override
	public void filter (ContainerRequestContext request, ContainerResponseContext response) {
		String contentType = getContentType(response.getMediaType());
		if (contentType != null) response.getHeaders().putSingle("Content-Type", contentType);
	}

	private String getContentType (MediaType type) {
		if (type == null) return null;
		String contentType = type.toString();
		return contentType.contains("charset") ? null : contentType + ";charset=UTF-8";
	}
}
