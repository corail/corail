package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {
	private Map<String, Object> config = new HashMap<>();

	private static Config instance = new Config();

	public Config () {
		try {
			Files
				.walk(Paths.get(this.getClass().getResource("properties/").toURI()))
				.filter(Files::isRegularFile)
				.forEach(this::getProperty);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String get (String key) {
		return (String) instance.config.get(key);
	}

	private void getProperty (Path path) {
		Properties properties = new Properties();
		InputStream input = null;
		String filename = path.getFileName().toString();

		try {
			input = new FileInputStream(path.toString());
			properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		Enumeration<?> e = properties.propertyNames();
		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			config.put(filename.replace(".properties", "") + "." + key, properties.getProperty(key));
		}
	}
}
