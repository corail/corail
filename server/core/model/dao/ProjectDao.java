package model.dao;


import model.pojo.ProjectPojo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectDao extends Dao<ProjectPojo> {
	private static ProjectDao instance = new ProjectDao();

	private ProjectDao () {
		super();
		tableName 		= "project";
		creationColumns = Arrays.asList("name", "type", "content", "image", "startedAt", "endedAt");
		editionColumns 	= creationColumns;
	}

	@Override
	ProjectPojo assign (ResultSet result) throws SQLException {
		return new ProjectPojo(result);
	}

	public static ProjectDao getInstance () {
		return instance;
	}

	@Override
	public ProjectPojo findById (int id) {
		ProjectPojo project = null;
		try {
			ResultSet result = findResultsById(id);
			if (!result.next()) return null;
			project = new ProjectPojo(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return project;
	}

	@Override
	public ProjectPojo create (ProjectPojo data) {
		ProjectPojo project = null;
		try {
			PreparedStatement statement = setStatement(prepareCreation(), data);
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			project = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return project;
	}

	@Override
	public ProjectPojo update (ProjectPojo data) {
		ProjectPojo project = null;
		int id = data.getId();
		try {
			setStatement(prepareEdition(id), data);
			project = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return project;
	}

	private PreparedStatement setStatement (PreparedStatement statement, ProjectPojo data) throws SQLException {
		statement.setString(1, data.getName());
		statement.setString(2, data.getType());
		statement.setString(3, data.getContent());
		statement.setString(4, data.getImage());
		statement.setTimestamp(5, data.getStartedAt());
		statement.setTimestamp(6, data.getEndedAt());
		statement.executeUpdate();
		return statement;
	}

	@Override
	public List<ProjectPojo> findAll () {
		List<ProjectPojo> projects = new ArrayList<>();
		try {
			ResultSet results = connection.createStatement().executeQuery(
				"SELECT " +
					"type, startedAt, endedAt, " +
					"p.id, p.name, p.content, p.image, p.createdAt, p.updatedAt, " +
					"COUNT(p.id) AS prototypesCount " +
				"FROM corail.project AS p " +
				"LEFT JOIN corail.prototype ON p.id = projectId " +
				"GROUP BY p.id"
			);
			while (results.next()) projects.add(new ProjectPojo(results));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return projects;
	}

	public ProjectPojo setImage (int id, String filename) {
		ProjectPojo project = null;
		try {
			PreparedStatement statement = connection.prepareStatement("UPDATE corail.project SET image=? WHERE id=?");
			statement.setString(1, filename);
			statement.setInt(2, id);
			statement.executeUpdate();
			project = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return project;
	}
}
