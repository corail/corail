package model.dao;

import model.pojo.CommentPojo;
import model.pojo.UserPojo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDao extends Dao<UserPojo> {
	private UserDao userDao = UserDao.getInstance();

	private UserDao () {
		super();
		super.tableName			= "user";
		super.creationColumns	= Arrays.asList("roleId", "firstname", "lastname", "email", "password");
		super.editionColumns	= Arrays.asList("roleId", "email", "password");
	}

	private static UserDao instance = new UserDao();

	public static UserDao getInstance () {
		return instance;
	}

	@Override
	UserPojo assign (ResultSet result) throws SQLException {
		return new UserPojo(result);
	}

	@Override
	public UserPojo findById (int id) {
		UserPojo user = null;
		try {
			ResultSet result = findResultsById(id);
			if (!result.next()) return null;
			user = new UserPojo(result);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public UserPojo create (UserPojo data) {
		UserPojo user	= null;
		try {
			PreparedStatement statement = prepareCreation();
			statement.setInt(1, data.getRoleId());
			statement.setString(2, data.getFirstname());
			statement.setString(3, data.getLastname());
			statement.setString(4, data.getEmail());
			statement.setString(5, data.getPassword());
			statement.executeUpdate();
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			user = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public UserPojo update (UserPojo data) {
		UserPojo user	= null;
		int id = data.getId();
		try {
			PreparedStatement preparedStatement = prepareEdition(id);
			preparedStatement.setInt(1, data.getRoleId());
			preparedStatement.setString(2, data.getEmail());
			preparedStatement.setString(3, data.getPassword());
			preparedStatement.executeUpdate();
			user = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public List<UserPojo> findAll () {
		List<UserPojo> users = new ArrayList<>();
		try {
			ResultSet results = connection.createStatement().executeQuery(
					"SELECT " +
							"id, roleId, firstname, lastname, email, createdAt " +
					"FROM corail.user"
			);
			while (results.next()) users.add(new UserPojo(results));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
}
