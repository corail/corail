package model.dao;

import model.Model;

import java.sql.*;
import java.util.*;

abstract class Dao<T> {
	Connection connection = null;
	String tableName;
	List<String> creationColumns;
	List<String> editionColumns;

	Dao () {
		try {
			connection = Model.getInstance().getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	ResultSet findResultsById (int id) throws SQLException {
		PreparedStatement statement	= connection.prepareStatement("SELECT * FROM " + tableName + " WHERE id=?");
		statement.setInt(1, id);
		return statement.executeQuery();
	}

	PreparedStatement prepareCreation () throws SQLException {
		String[] valuesEscaped = new String[creationColumns.size()];
		Arrays.fill(valuesEscaped, "?");
		return connection.prepareStatement(
			"INSERT INTO " + tableName + formatList(creationColumns) +
				" VALUES " + formatList(Arrays.asList(valuesEscaped)),
			Statement.RETURN_GENERATED_KEYS
		);
	}

	PreparedStatement prepareEdition (int id) throws SQLException {
		PreparedStatement statement = connection.prepareStatement("UPDATE " + tableName + " SET " +
			String.join("=?, ", editionColumns) + "=? WHERE id=?");
		statement.setInt(editionColumns.size() + 1, id);
		return statement;
	}

	public List<T> findAll () {
		List<T> comments = new ArrayList<>();
		try {
			ResultSet results = connection.createStatement().executeQuery("SELECT * FROM " + tableName);
			while (results.next()) comments.add(assign(results));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comments;
	}

	public boolean delete (int id) {
		try {
			PreparedStatement statement = connection.prepareStatement("DELETE FROM " + tableName + " WHERE id=?");
			statement.setInt(1, id);
			statement.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	abstract T assign (ResultSet result) throws SQLException;
	public abstract T findById (int id);
	public abstract T create (T data);
	public abstract T update (T data);

	private String formatList (List<String> list) {
		return "(" + String.join(", ", list) + ")";
	}
}
