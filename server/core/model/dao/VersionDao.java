package model.dao;

import model.pojo.UserPojo;
import model.pojo.VersionPojo;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VersionDao extends Dao<VersionPojo>{
	private static VersionDao instance = new VersionDao();

	public VersionDao() {
		super();
		tableName 		= "version";
		creationColumns = Arrays.asList("prototypeId", "userId", "name", "tag", "content");
		editionColumns 	= creationColumns;
	}

	@Override
	VersionPojo assign (ResultSet result) throws SQLException {
		return new VersionPojo(result);
	}

	public static VersionDao getInstance () {
		return instance;
	}

	@Override
	public VersionPojo findById (int id) {
		VersionPojo version = null;
		try {
			PreparedStatement statement	= connection.prepareStatement(
				"SELECT " +
					"v.id, v.userId, v.prototypeId, v.name, v.content, v.tag, v.createdAt, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.version AS v " +
				"LEFT JOIN corail.user AS u " +
					"ON u.id=v.userId " +
				"WHERE v.id=?"
			);
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (!result.next()) return null;
			UserPojo user = new UserPojo();
			version = new VersionPojo(result);
			user.setFirstname(result.getString("userFirstname"));
			user.setLastname(result.getString("userLastname"));
			version.setUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return version;
	}

	@Override
	public VersionPojo create (VersionPojo data) {
		VersionPojo version= null;
		try {
			PreparedStatement statement = setStatement(prepareCreation(), data);
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			version = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return version;
	}

	@Override
	public VersionPojo update (VersionPojo data) {
		VersionPojo version = null;
		int id = data.getId();
		try {
			setStatement(prepareEdition(id), data);
			version = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return version;
	}

	public List<VersionPojo> findAll (int prototypeId) {
		List<VersionPojo> versions = new ArrayList<>();
		try {
			PreparedStatement statement = connection.prepareStatement(
				"SELECT " +
					"v.id, v.prototypeId, v.userId, v.name, v.content, v.createdAt, v.tag, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM version AS v " +
				"LEFT JOIN corail.user AS u " +
					"ON u.id=v.userId " +
				"WHERE prototypeId=?"
			);
			statement.setInt(1, prototypeId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				UserPojo user = new UserPojo();
				VersionPojo version = assign(results);
				user.setFirstname(results.getString("userFirstname"));
				user.setLastname(results.getString("userLastname"));
				version.setUser(user);
				versions.add(version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return versions;
	}

	private PreparedStatement setStatement (PreparedStatement statement, VersionPojo data) throws SQLException {
		statement.setInt(1, data.getPrototypeId());
		statement.setInt(2, data.getUserId());
		statement.setString(3, data.getName());
		statement.setString(4, data.getTag());
		statement.setString(5, data.getContent());
		statement.executeUpdate();
		return statement;
	}

	public VersionPojo setImage (VersionPojo version, String filename) {
		try {
			PreparedStatement statement = connection.prepareStatement("UPDATE corail.prototype SET image=? WHERE id=?");
			statement.setString(1, filename);
			statement.setInt(2, version.getPrototypeId());
			statement.executeUpdate();
			version = findById(version.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return version;
	}
}
