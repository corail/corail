package model.dao;


import model.pojo.PrototypePojo;
import model.pojo.UserPojo;
import model.pojo.VersionPojo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class PrototypeDao extends Dao<PrototypePojo>{
	private static PrototypeDao instance = new PrototypeDao();

	public PrototypeDao() {
		super();
		tableName 		= "prototype";
		creationColumns = Arrays.asList("projectId", "name", "content", "status");
		editionColumns 	= Arrays.asList("name", "content", "status");
	}

	@Override
	PrototypePojo assign (ResultSet result) throws SQLException {
		return new PrototypePojo(result);
	}

	public static PrototypeDao getInstance () {
		return instance;
	}

	@Override
	public PrototypePojo findById (int id) {
		PrototypePojo prototype = null;
		try {
			PreparedStatement statement	= connection.prepareStatement(
				"SELECT " +
					"p1.id, p1.projectId, p1.name, p1.content, p1.image, p1.status, p1.createdAt, p1.updatedAt, " +
					"MIN(p2.id) AS previousPrototypeId, MAX(p3.id) AS nextPrototypeId, " +
					"v.id AS versionId, v.userId AS versionUserId, v.name AS versionName, v.content AS versionContent, v.tag AS versionTag, v.createdAt AS versionCreatedAt, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.prototype AS p1 " +
				"LEFT JOIN corail.prototype AS p2 " +
					"ON p2.projectId=p1.projectId AND p2.id<=p1.id " +
				"LEFT JOIN corail.prototype AS p3 " +
					"ON p3.projectId=p1.projectId AND p3.id>=p1.id " +
				"LEFT JOIN corail.version AS v " +
					"ON v.prototypeId=p1.id AND v.createdAt IN (NULL, (SELECT MAX(createdAt) FROM corail.version WHERE prototypeId=p1.id)) " +
				"LEFT JOIN corail.user AS u " +
					"ON u.id=v.userId " +
				"WHERE p1.id=? " +
				"GROUP BY p1.id, p1.projectId, v.id"
			);
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (!result.next()) return null;
			prototype = assign(result);
			int previousPrototypeId = result.getInt("previousPrototypeId");
			int nextPrototypeId = result.getInt("nextPrototypeId");
			int versionId = result.getInt("versionId");
			if (previousPrototypeId != id) prototype.setPreviousPrototypeId(previousPrototypeId);
			if (nextPrototypeId != id) prototype.setNextPrototypeId(nextPrototypeId);
			if (versionId != 0) {
				UserPojo user = new UserPojo();
				VersionPojo version = new VersionPojo();
				user.setFirstname(result.getString("userFirstname"));
				user.setLastname(result.getString("userLastname"));
				version.setId(versionId);
				version.setUserId(result.getInt("versionUserId"));
				version.setName(result.getString("versionName"));
				version.setContent(result.getString("versionContent"));
				version.setTag(result.getString("versionTag"));
				version.setCreatedAt(result.getTimestamp("versionCreatedAt"));
				version.setUser(user);
				prototype.setVersion(version);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return prototype;
	}

	@Override
	public PrototypePojo create (PrototypePojo data) {
		PrototypePojo prototype = null;
		try {
			PreparedStatement statement = prepareCreation();
			statement.setInt(1, data.getProjectId());
			statement.setString(2, data.getName());
			statement.setString(3, data.getContent());
			statement.setString(4, data.getStatus());
			statement.executeUpdate();
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			prototype = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return prototype;
	}

	@Override
	public PrototypePojo update (PrototypePojo data) {
		PrototypePojo prototype = null;
		int id = data.getId();
		try {
			PreparedStatement statement = prepareEdition(id);
			statement.setString(1, data.getName());
			statement.setString(2, data.getContent());
			statement.setString(3, data.getStatus());
			statement.executeUpdate();
			prototype = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return prototype;
	}


	public List<PrototypePojo> findAll (int projectId) {
		List<PrototypePojo> prototypes = new ArrayList<>();
		try {
			PreparedStatement statement = connection.prepareStatement(
				"SELECT " +
					"p.id, p.projectId, p.name, p.content, p.image, p.status, p.createdAt, p.updatedAt, " +
					"v.id AS versionId, v.userId AS versionUserId, v.name AS versionName, v.tag AS versionTag, v.createdAt AS versionCreatedAt, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.prototype AS p " +
				"LEFT JOIN corail.version AS v " +
					"ON v.prototypeId=p.id AND v.createdAt IN (NULL, (SELECT MAX(createdAt) FROM corail.version WHERE prototypeId=p.id)) " +
				"LEFT JOIN corail.user AS u " +
					"ON u.id=v.userId " +
				"WHERE projectId=?"
			);
			statement.setInt(1, projectId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				PrototypePojo prototype = assign(results);
				int versionId = results.getInt("versionId");
				if (versionId != 0) {
					UserPojo user = new UserPojo();
					VersionPojo version = new VersionPojo();
					user.setFirstname(results.getString("userFirstname"));
					user.setLastname(results.getString("userLastname"));
					version.setId(versionId);
					version.setUserId(results.getInt("versionUserId"));
					version.setName(results.getString("versionName"));
					version.setTag(results.getString("versionTag"));
					version.setCreatedAt(results.getTimestamp("versionCreatedAt"));
					version.setUser(user);
					prototype.setVersion(version);
				}
				prototypes.add(prototype);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return prototypes;
	}
}
