package model.dao;

import model.pojo.CommentPojo;
import model.pojo.ResponsePojo;
import model.pojo.UserPojo;

import javax.xml.ws.Response;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class CommentDao extends Dao<CommentPojo> {
	private ResponseDao responseDao = ResponseDao.getInstance();

	private CommentDao () {
		super();
		super.tableName			= "comment";
		super.creationColumns	= Arrays.asList("prototypeId", "userId", "content", "isPrivate", "positionX", "positionY");
		super.editionColumns	= Arrays.asList("content", "isPrivate", "isProcessed");
	}

	private static CommentDao instance = new CommentDao();

	public static CommentDao getInstance () {
		return instance;
	}

	@Override
	CommentPojo assign (ResultSet result) throws SQLException {
		return new CommentPojo(result);
	}

	@Override
	public CommentPojo findById (int id) {
		CommentPojo comment = null;
		try {
			PreparedStatement statement	= connection.prepareStatement(
				"SELECT " +
					"c.id, c.prototypeId, c.userId, c.content, c.isProcessed, c.isPrivate, c.createdAt, c.updatedAt, c.endedAt, c.positionX, c.positionY, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.comment AS c " +
				"LEFT JOIN corail.user AS u ON u.id=c.userId " +
				"WHERE c.id=?"
			);
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (!result.next()) return null;
			UserPojo user = new UserPojo();
			comment = new CommentPojo(result);
			user.setFirstname(result.getString("userFirstname"));
			user.setLastname(result.getString("userLastname"));
			comment.setUser(user);
			comment.setResponses(responseDao.findAll(comment.getId()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comment;
	}

	@Override
	public CommentPojo create (CommentPojo data) {
		CommentPojo comment	= null;
		try {
			PreparedStatement statement = prepareCreation();
			statement.setInt(1, data.getPrototypeId());
			statement.setInt(2, data.getUserId());
			statement.setString(3, data.getContent());
			statement.setBoolean(4, data.getIsPrivate());
			statement.setInt(5, data.getPositionX());
			statement.setInt(6, data.getPositionY());
			statement.executeUpdate();
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			comment = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comment;
	}

	@Override
	public CommentPojo update (CommentPojo data) {
		CommentPojo comment	= null;
		int id = data.getId();
		try {
			PreparedStatement preparedStatement = prepareEdition(id);
			preparedStatement.setString(1, data.getContent());
			preparedStatement.setBoolean(2, data.getIsPrivate());
			preparedStatement.setBoolean(3, data.getIsProcessed());
			preparedStatement.executeUpdate();
			comment = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comment;
	}

	public List<CommentPojo> findAll (int prototypeId) {
		Map<Number, CommentPojo> comments = new HashMap<>();
		try {
			PreparedStatement statement = connection.prepareStatement(
				"SELECT " +
					"c.id, c.prototypeId, c.userId, c.content, c.isProcessed, c.isPrivate, c.createdAt, c.updatedAt, c.endedAt, c.positionX, c.positionY, " +
					"r.id AS r_id, r.userId AS r_userId, r.content AS r_content, r.createdAt AS r_createdAt, " +
					"u0.firstname AS commentUserFirstname, u0.lastname AS commentUserLastname, " +
					"u1.firstname AS responseUserFirstname, u1.lastname AS responseUserLastname " +
				"FROM corail.comment AS c " +
				"LEFT JOIN response AS r ON c.id=r.commentId " +
				"LEFT JOIN corail.user AS u0 ON u0.id=c.userId " +
				"LEFT JOIN corail.user AS u1 ON u1.id=r.userId " +
				"WHERE prototypeId=?"
			);
			statement.setInt(1, prototypeId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				int id = results.getInt("id");
				CommentPojo comment;
				List<ResponsePojo> responses;
				if (comments.containsKey(id)) {
					comment = comments.get(id);
					responses = comment.getResponses();
				} else {
					UserPojo user = new UserPojo();
					comment = new CommentPojo(results);
					user.setFirstname(results.getString("commentUserFirstname"));
					user.setLastname(results.getString("commentUserLastname"));
					responses = new ArrayList<>();
					comment.setResponses(responses);
					comment.setUser(user);
					comments.put(id, comment);
				}
				int responseId = results.getInt("r_id");
				if (responseId > 0) {
					UserPojo user = new UserPojo();
					ResponsePojo response = new ResponsePojo();
					user.setFirstname(results.getString("responseUserFirstname"));
					user.setLastname(results.getString("responseUserLastname"));
					response.setId(responseId);
					response.setUserId(results.getInt("r_userId"));
					response.setContent(results.getString("r_content"));
					response.setCreatedAt(results.getTimestamp("r_createdAt"));
					response.setUser(user);
					responses.add(response);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<>(comments.values());
	}
}
