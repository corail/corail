package model.dao;

import model.pojo.ResponsePojo;
import model.pojo.UserPojo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResponseDao extends Dao<ResponsePojo> {
	private static ResponseDao instance = new ResponseDao();

	private ResponseDao () {
		super();
		super.tableName			= "response";
		super.creationColumns	= Arrays.asList("commentId", "userId", "content");
		super.editionColumns	= Arrays.asList("content");
	}

	public static ResponseDao getInstance () {
		return instance;
	}

	@Override
	ResponsePojo assign (ResultSet result) throws SQLException {
		return new ResponsePojo(result);
	}

	@Override
	public ResponsePojo findById (int id) {
		try {
			PreparedStatement statement	= connection.prepareStatement(
				"SELECT " +
					"r.id, r.commentId, r.userId, r.content, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.response AS r " +
				"LEFT JOIN corail.user AS u ON u.id=r.userId " +
				"WHERE r.id=?"
			);
			statement.setInt(1, id);
			ResultSet result = statement.executeQuery();
			if (!result.next()) return null;
			UserPojo user = new UserPojo();
			ResponsePojo response = new ResponsePojo(result);
			user.setFirstname(result.getString("userFirstname"));
			user.setLastname(result.getString("userLastname"));
			response.setUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public ResponsePojo create (ResponsePojo data) {
		ResponsePojo comment = null;
		try {
			PreparedStatement statement = prepareCreation();
			statement.setInt(1, data.getCommentId());
			statement.setInt(2, data.getUserId());
			statement.setString(3, data.getContent());
			statement.executeUpdate();
			ResultSet result = statement.getGeneratedKeys();
			if (!result.next()) return null;
			comment = findById(result.getInt(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comment;
	}

	@Override
	public ResponsePojo update (ResponsePojo data) {
		ResponsePojo comment = null;
		int id = data.getId();
		try {
			PreparedStatement preparedStatement = prepareEdition(id);
			preparedStatement.setString(1, data.getContent());
			preparedStatement.executeUpdate();
			comment = findById(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return comment;
	}

	public List<ResponsePojo> findAll (int commentId) {
		List<ResponsePojo> responses = new ArrayList<>();
		try {
			PreparedStatement statement = connection.prepareStatement(
				"SELECT " +
					"r.id, r.commentId, r.userId, r.content, " +
					"u.firstname AS userFirstname, u.lastname AS userLastname " +
				"FROM corail.response AS r " +
				"LEFT JOIN corail.user AS u ON u.id=r.userId " +
				"WHERE r.commentId=?"
			);
			statement.setInt(1, commentId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				UserPojo user = new UserPojo();
				ResponsePojo response = assign(results);
				user.setFirstname(results.getString("userFirstname"));
				user.setLastname(results.getString("userLastname"));
				response.setUser(user);
				responses.add(response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return responses;
	}
}
