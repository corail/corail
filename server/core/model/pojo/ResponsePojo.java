package model.pojo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class ResponsePojo {
	private int id;
	private int commentId;
	private int userId;
	private UserPojo user;
	private String content;
	private Timestamp createdAt;

	public ResponsePojo () {}

	public ResponsePojo (ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setCommentId(result.getInt("commentId"));
		setUserId(result.getInt("userId"));
		setContent(result.getString("content"));
		setCreatedAt(result.getTimestamp("createdAt"));
	}

	public int getId () {
		return id;
	}

	public int getCommentId () {
		return commentId;
	}

	public int getUserId () {
		return userId;
	}

	public UserPojo getUser () {
		return user;
	}

	public String getContent () {
		return content;
	}

	public Timestamp getCreatedAt () {
		return createdAt;
	}

	public void setId (int id) {
		this.id = id;
	}

	public void setCommentId (int commentId) {
		this.commentId = commentId;
	}

	public void setUserId (int userId) {
		this.userId = userId;
	}

	public void setUser (UserPojo user) {
		this.user = user;
	}

	public void setContent (String content) {
		this.content = content;
	}

	public void setCreatedAt (Timestamp createdAt) {
		this.createdAt = createdAt;
	}
}
