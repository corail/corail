package model.pojo;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class PrototypePojo extends Pojo {
	private int id;
	private int projectId;
	private int previousPrototypeId;
	private int nextPrototypeId;
	private VersionPojo version;
	private String name;
	private String content;
	private String image;
	private String status;
	private Timestamp createdAt;
	private Timestamp updatedAt;

	public PrototypePojo() {}

	public PrototypePojo(ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setProjectId(result.getInt("projectId"));
		setName(result.getString("name"));
		setContent(result.getString("content"));
		setImage(result.getString("image"));
		setStatus(result.getString("status"));
		setCreatedAt(result.getTimestamp("createdAt"));
		setUpdatedAt(result.getTimestamp("updatedAt"));
	}

	public int getId() {
		return id;
	}

	public int getProjectId() {
		return projectId;
	}

	public int getPreviousPrototypeId() {
		return previousPrototypeId;
	}

	public int getNextPrototypeId() {
		return nextPrototypeId;
	}

	public VersionPojo getVersion() {
		return version;
	}

	public String getName() {
		return name;
	}

	public String getContent() {
		return content;
	}

	public String getImage () {
		return image;
	}

	public String getStatus() {
		return status;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public void setPreviousPrototypeId(int previousPrototypeId) {
		this.previousPrototypeId = previousPrototypeId;
	}

	public void setNextPrototypeId(int nextPrototypeId) {
		this.nextPrototypeId = nextPrototypeId;
	}

	public void setVersion(VersionPojo version) {
		this.version = version;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setImage (String image) {
		this.image = image;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
}
