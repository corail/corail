package model.pojo;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

public class ProjectPojo extends Pojo {
	private int id;
	private String name;
	private String type;
	private String content;
	private String image;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Timestamp startedAt;
	private Timestamp endedAt;
	private String status;
	private int prototypesCount;

	public ProjectPojo () {}

	public ProjectPojo (ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setName(result.getString("name"));
		setType(result.getString("type"));
		setContent(result.getString("content"));
		setImage(result.getString("image"));
		setCreatedAt(result.getTimestamp("createdAt"));
		setUpdatedAt(result.getTimestamp("updatedAt"));
		setStartedAt(result.getTimestamp("startedAt"));
		setEndedAt(result.getTimestamp("endedAt"));

		try {
			setPrototypesCount(result.getInt("prototypesCount"));
		} catch (SQLException e) {}

		// Calculated field `status`
		Timestamp startedAt	= getStartedAt();
		Timestamp endedAt	= getEndedAt();
		Date date			= new Date(Calendar.getInstance().getTime().getTime());
		String status;
		if (endedAt != null && endedAt.compareTo(date) < 0) status = "done";
		else if (startedAt != null && startedAt.compareTo(date) < 0) status = "in_progress";
		else status = "started";
		setStatus(status);
	}

	public int getId () {
		return id;
	}

	public String getName () {
		return name;
	}

	public String getType () {
		return type;
	}

	public String getContent () {
		return content;
	}

	public String getImage () {
		return image;
	}

	public Timestamp getCreatedAt () {
		return createdAt;
	}

	public Timestamp getUpdatedAt () {
		return updatedAt;
	}

	public Timestamp getStartedAt () {
		return startedAt;
	}

	public Timestamp getEndedAt () {
		return endedAt;
	}

	public String getStatus () {
		return status;
	}

	public int getPrototypesCount () {
		return prototypesCount;
	}

	public void setId (int id) {
		this.id = id;
	}

	public void setName (String name) {
		this.name = name;
	}

	public void setType (String type) {
		this.type = type;
	}

	public void setContent (String content) {
		this.content = content;
	}

	public void setImage (String image) {
		this.image = image;
	}

	public void setCreatedAt (Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt (Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setStartedAt (Timestamp startedAt) {
		this.startedAt = startedAt;
	}

	public void setEndedAt (Timestamp endedAt) {
		this.endedAt = endedAt;
	}

	public void setStatus (String status) {
		this.status = status;
	}

	public void setPrototypesCount (int prototypesCount) {
		this.prototypesCount = prototypesCount;
	}
}
