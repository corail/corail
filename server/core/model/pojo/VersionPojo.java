package model.pojo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class VersionPojo extends Pojo  {
	private int id;
	private int prototypeId;
	private int userId;
	private UserPojo user;
	private String name;
	private String content;
	private String tag;
	private Timestamp createdAt;


	public VersionPojo () {}

	public VersionPojo (ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setPrototypeId(result.getInt("prototypeId"));
		setUserId(result.getInt("userId"));
		setName(result.getString("name"));
		setContent(result.getString("content"));
		setCreatedAt(result.getTimestamp("createdAt"));
		setTag(result.getString("tag"));
	}

	public int getId () {
		return id;
	}

	public int getPrototypeId () {
		return prototypeId;
	}

	public int getUserId () {
		return userId;
	}

	public UserPojo getUser () {
		return user;
	}

	public String getName () {
		return name;
	}

	public String getContent () {
		return content;
	}

	public String getTag () {
		return tag;
	}

	public Timestamp getCreatedAt () {
		return createdAt;
	}

	public void setId (int id) {
		this.id = id;
	}

	public void setPrototypeId (int prototypeId) {
		this.prototypeId = prototypeId;
	}

	public void setUserId (int userId) {
		this.userId = userId;
	}

	public void setUser (UserPojo user) {
		this.user = user;
	}

	public void setName (String name) {
		this.name = name;
	}

	public void setContent (String content) {
		this.content = content;
	}

	public void setTag (String tag) {
		this.tag = tag;
	}

	public void setCreatedAt (Timestamp createdAt) {
		this.createdAt = createdAt;
	}

}
