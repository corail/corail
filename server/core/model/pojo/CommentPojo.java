package model.pojo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class CommentPojo extends Pojo {
	private int id;
	private int prototypeId;
	private int userId;
	private UserPojo user;
	private String content;
	private boolean isProcessed;
	private boolean isPrivate;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Timestamp endedAt;
	private int positionX;
	private int positionY;
	private List<ResponsePojo> responses;

	public CommentPojo () {}

	public CommentPojo (ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setPrototypeId(result.getInt("prototypeId"));
		setUserId(result.getInt("userId"));
		setContent(result.getString("content"));
		setIsProcessed(result.getBoolean("isProcessed"));
		setIsPrivate(result.getBoolean("isPrivate"));
		setCreatedAt(result.getTimestamp("createdAt"));
		setUpdatedAt(result.getTimestamp("updatedAt"));
		setEndedAt(result.getTimestamp("endedAt"));
		setPositionX(result.getInt("positionX"));
		setPositionY(result.getInt("positionY"));
	}

	public void setId (int id) {
		this.id = id;
	}

	public void setPrototypeId (int prototypeId) {
		this.prototypeId = prototypeId;
	}

	public void setUserId (int userId) {
		this.userId = userId;
	}

	public void setUser (UserPojo user) {
		this.user = user;
	}

	public void setContent (String content) {
		this.content = content;
	}

	public void setIsProcessed (boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	public void setIsPrivate (boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public void setCreatedAt (Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt (Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setEndedAt (Timestamp endedAt) {
		this.endedAt = endedAt;
	}

	public void setPositionX (int positionX) {
		this.positionX = positionX;
	}

	public void setPositionY (int positionY) {
		this.positionY = positionY;
	}

	public void setResponses (List<ResponsePojo> responses) {
		this.responses = responses;
	}

	public int getId () {
		return id;
	}

	public int getPrototypeId () {
		return prototypeId;
	}

	public int getUserId () {
		return userId;
	}

	public UserPojo getUser () {
		return user;
	}

	public String getContent () {
		return content;
	}

	public boolean getIsProcessed () {
		return isProcessed;
	}

	public boolean getIsPrivate () {
		return isPrivate;
	}

	public Timestamp getCreatedAt () {
		return createdAt;
	}

	public Timestamp getUpdatedAt () {
		return updatedAt;
	}

	public Timestamp getEndedAt () {
		return endedAt;
	}

	public int getPositionX () {
		return positionX;
	}

	public int getPositionY () {
		return positionY;
	}

	public List<ResponsePojo> getResponses () {
		return responses;
	}
}
