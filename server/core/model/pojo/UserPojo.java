package model.pojo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class UserPojo extends Pojo {
	private int id;
	private int roleId;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private Timestamp createdAt;
	private List<ResponsePojo> responses;

	public UserPojo () {}

	public UserPojo (ResultSet result) throws SQLException {
		setId(result.getInt("id"));
		setRoleId(result.getInt("roleId"));
		setFirstname(result.getString("firstname"));
		setLastname(result.getString("lastname"));
		setEmail(result.getString("email"));
		setCreatedAt(result.getTimestamp("createdAt"));
	}

	public List<ResponsePojo> getResponses() {
		return responses;
	}

	public void setResponses(List<ResponsePojo> responses) {
		this.responses = responses;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}


}
