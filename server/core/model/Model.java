package model;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import config.Config;

import java.sql.Connection;
import java.sql.SQLException;

public class Model {
	private BoneCP connectionPool;

	public Model (BoneCP connectionPool) {
		this.connectionPool = connectionPool;
	}

	public Connection getConnection () throws SQLException {
		return connectionPool.getConnection();
	}

	public static Model getInstance () {
		BoneCP connectionPool = null;
		try {
			// Tell the server which driver to use
			Class.forName("com.mysql.jdbc.Driver");

			BoneCPConfig config = new BoneCPConfig();
			config.setJdbcUrl(Config.get("database.url"));
			config.setUsername(Config.get("database.user"));
			config.setPassword(Config.get("database.password"));
			config.setMinConnectionsPerPartition(5);
			config.setMaxConnectionsPerPartition(10);
			config.setPartitionCount(2);
			connectionPool = new BoneCP(config);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new Model(connectionPool);
	}
}
