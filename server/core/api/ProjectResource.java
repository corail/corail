package api;

import model.dao.ProjectDao;
import model.pojo.ProjectPojo;
import org.glassfish.jersey.media.multipart.FormDataParam;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Path("projects")
@Produces(MediaType.APPLICATION_JSON)
public class ProjectResource {
	@Context ServletContext context;

	private ProjectDao projectService = ProjectDao.getInstance();
	private java.nio.file.Path imageDirectory;

	@PostConstruct
	public void postConstruct () {
		try {
			imageDirectory = Paths.get(context.getRealPath("/") + "images/project");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	public List<ProjectPojo> findAll () {
		return projectService.findAll();
	}

	@GET
	@Path("{id: \\d+}")
	public ProjectPojo findById (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		ProjectPojo project = projectService.findById(id);
		if (project == null) throw new NotFoundException();
		return project;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ProjectPojo create (ProjectPojo project) {
		return projectService.create(project);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public ProjectPojo update (ProjectPojo project) {
		if (project.getId() <= 0) throw new NotFoundException();
		project = projectService.update(project);
		if (project == null) throw new NotFoundException();
		return project;
	}

	@DELETE
	@Path("{id: \\d+}")
	public void delete (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		boolean deleted = projectService.delete(id);
		if (!deleted) throw new NotFoundException();
	}

	@POST
	@Path("{id: \\d+}/image")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public ProjectPojo upload (@PathParam("id") int id, @FormDataParam("file") InputStream file) {
		String filename = UUID.randomUUID().toString();
		OutputStream output = null;
		try	{
			output = new FileOutputStream(new File(imageDirectory.toString() + "/" + filename + ".jpg"));
			byte[] bytes = new byte[2048];
			int read;
			while ((read = file.read(bytes)) != -1) {
				output.write(bytes, 0, read);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return projectService.setImage(id, filename);
	}
}
