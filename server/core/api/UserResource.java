package api;

import model.dao.UserDao;
import model.pojo.UserPojo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
	private UserDao userService = UserDao.getInstance();

	@GET
	@Path("{id: \\d+}")
	public UserPojo getOne (@PathParam("id") int id) {
		return userService.findById(id);
	}

	@GET
	public List<UserPojo> getAll () {
		return userService.findAll();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public UserPojo create (UserPojo user) {
		return userService.create(user);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public UserPojo update (UserPojo user) {
		if (user.getId() <= 0) throw new BadRequestException();
		user = userService.update(user);
		if (user == null) throw new NotFoundException();
		return user;
	}

	@DELETE
	@Path("{id: \\d+}")
	public void delete (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		boolean deleted = userService.delete(id);
		if (!deleted) throw new NotFoundException();
	}
}
