package api;

import model.dao.CommentDao;
import model.pojo.CommentPojo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("comments")
@Produces(MediaType.APPLICATION_JSON)
public class CommentResource {
	private CommentDao commentService = CommentDao.getInstance();

	@GET
	@Path("{id: \\d+}")
	public List<CommentPojo> getAll (@PathParam("id") int id) {
		return commentService.findAll(id);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public CommentPojo create (CommentPojo comment) {
		return commentService.create(comment);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public CommentPojo update (CommentPojo comment) {
		if (comment.getId() <= 0) throw new BadRequestException();
		comment = commentService.update(comment);
		if (comment == null) throw new NotFoundException();
		return comment;
	}

	@DELETE
	@Path("{id: \\d+}")
	public void delete (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		boolean deleted = commentService.delete(id);
		if (!deleted) throw new NotFoundException();
	}
}
