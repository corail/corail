package api;

import model.dao.ResponseDao;
import model.pojo.ResponsePojo;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("responses")
@Produces(MediaType.APPLICATION_JSON)
public class ResponseResource {
	private ResponseDao responseService = ResponseDao.getInstance();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public ResponsePojo create (ResponsePojo response) {
		return responseService.create(response);
	}
}
