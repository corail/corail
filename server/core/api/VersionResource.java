package api;

import model.dao.VersionDao;
import model.pojo.VersionPojo;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Path("versions")
@Produces(MediaType.APPLICATION_JSON)
public class VersionResource {
	@Context ServletContext context;

	private VersionDao versionService = VersionDao.getInstance();
	private java.nio.file.Path imageDirectory;

	@PostConstruct
	public void postConstruct () {
		try {
			imageDirectory = Paths.get(context.getRealPath("/") + "images/prototype");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("{id: \\d+}")
	public List<VersionPojo> getAll (@PathParam("id") int id) {
		return versionService.findAll(id);
	}

	@GET
	@Path("version/{id: \\d+}")
	public VersionPojo get (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		VersionPojo version = versionService.findById(id);
		if (version == null) throw new NotFoundException();
		return version;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionPojo create (VersionPojo version) {
		return generate(versionService.create(version));
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public VersionPojo update (VersionPojo version) {
		if (version.getId() <= 0) throw new BadRequestException();
		version = versionService.update(version);
		if (version == null) throw new NotFoundException();
		return generate(version);
	}

	@DELETE
	@Path("{id: \\d+}")
	public void delete (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		boolean deleted = versionService.delete(id);
		if (!deleted) throw new NotFoundException();
	}

	private VersionPojo generate (VersionPojo version) {
		if (version.getContent().equals("")) return version;
		String filename = UUID.randomUUID().toString();
		try {
			Runtime.getRuntime().exec("wkhtmltoimage --quality 100 \"http://localhost:8080/api/prototypes/" + version.getPrototypeId() + "/preview\" " + imageDirectory.toString() + "/" + filename + ".jpg");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			version = versionService.setImage(version, filename);
		}
		return version;
	}
}
