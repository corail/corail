package api;


import model.dao.PrototypeDao;
import model.pojo.PrototypePojo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.lf5.util.StreamUtils;
import org.glassfish.jersey.server.mvc.Template;

@Path("prototypes")
@Produces(MediaType.APPLICATION_JSON)
public class PrototypeResource {
	private PrototypeDao prototypeService = PrototypeDao.getInstance();

	@GET
	@Path("{id: \\d+}")
	public List<PrototypePojo> getAll (@PathParam("id") int id) {
		return prototypeService.findAll(id);
	}

	@GET
	@Path("prototype/{id: \\d+}")
	public PrototypePojo get (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		PrototypePojo prototype = prototypeService.findById(id);
		if (prototype == null) throw new NotFoundException();
		return prototype;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public PrototypePojo create (PrototypePojo prototype) {
		return prototypeService.create(prototype);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public PrototypePojo update (PrototypePojo prototype) {
		if (prototype.getId() <= 0) throw new BadRequestException();
		prototype = prototypeService.update(prototype);
		if (prototype == null) throw new NotFoundException();
		return prototype;
	}

	@DELETE
	@Path("{id: \\d+}")
	public void delete (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		boolean deleted = prototypeService.delete(id);
		if (!deleted) throw new NotFoundException();
	}

	@GET
	@Path("{id: \\d+}/preview")
	@Produces(MediaType.TEXT_HTML)
	@Template(name = "/prototypePreview")
	public Map<String, Object> preview (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		PrototypePojo prototype = prototypeService.findById(id);
		if (prototype == null || prototype.getVersion() == null) throw new NotFoundException();
		Map<String, Object> model = new HashMap<>();
		model.put("name", prototype.getName());
		model.put("description", prototype.getContent());
		model.put("content", prototype.getVersion().getContent());
		return model;
	}

	@GET
	@Path("{id: \\d+}/export/pdf")
	@Produces("application/pdf")
	public StreamingOutput exportPDF (@PathParam("id") int id) {
		if (id == 0) throw new NotFoundException();
		PrototypePojo prototype = prototypeService.findById(id);
		if (prototype == null || prototype.getVersion() == null) throw new NotFoundException();
		String filename = UUID.randomUUID().toString();
		return output -> {
			File download;
			try {
				download = File.createTempFile(filename, ".pdf");
				Process process = Runtime.getRuntime().exec("wkhtmltopdf --viewport-size 1280x960 -B 0 -L 0 -R 0 -T 0 \"http://localhost:8080/api/prototypes/" + prototype.getId() + "/preview\" " + download.getPath());
				int status = process.waitFor();
				if (status == 0) StreamUtils.copy(new FileInputStream(download), output);
				else System.out.println("Error creating the PDF file: " + download.getPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	}
}
