'use strict'

const path			= require('path')
const gulp			= require('gulp')
const lazy			= require('lazypipe')
const addStream		= require('add-stream')
const proxy			= require('http-proxy-middleware')
const browserSync	= require('browser-sync').create()
const properties	= require('./package')

const config = {
	dependencies: [
		'./package.json',
		'./client/bower.json'
	],
	index: {
		source:			'./client/index.html',
		destination:	'./public'
	},
	js: {
		source:			'./client/{core,components}/**/*.js',
		destination:	'./public/js',
		filename:		'application.js',
		header:			'"use strict";'
	},
	template: {
		source:			'./client/{core,components}/**/*.html'
	},
	css: {
		source:			'./client/assets/scss/**/*.scss',
		destination:	'./public/css',
		filename:		'application.css'
	},
	image: {
		source:				'./client/assets/images/**',
		destination:		'./public/images'
	},
	font: {
		source:				'./client/{assets,imports}/**/fonts/**/*.{eot,otf,ttf,svg,woff,woff2}',
		destination:		'./public/fonts'
	},

	htmlmin: {
		collapseWhitespace:		true,
		conservativeCollapse:	true,
		removeTagWhitespace:	true,
		removeAttributeQuotes:	true
	},
	uglify: {},
	ngTemplatecache: {
		module: properties.name
	},
	babel: {
		compact: true
	},
	eslint: {
		configFile: './client/.eslintrc'
	},
	sass: {
		includePaths: ['./client/imports/foundation-sites/scss']
	},
	autoprefixer: {
		cascade: false,
		remove: false
	},
	csso: {},

	browserSync: {
		server: {
			baseDir: './public'
		}
	},

	errorHandler: {
		babel (err) {
			console.log(`\n\u001b[31m${err.name}\u001b[0m in plugin \'\u001b[36m${err.plugin}\u001b[0m\'`)
			console.log(err.message)
			console.log(`${err.codeFrame}\n`)
			this.emit('end')
		},
		sass (err) {
			console.log(`\n\u001b[31m${err.name}\u001b[0m in plugin \'\u001b[36m${err.plugin}\u001b[0m\'`)
			console.log(`${path.join(__dirname, err.relativePath).replace(/\\/g, '/')}: ${err.messageOriginal.replace(/ +/g, ' ')} (${err.line}:${err.column})`)
			console.log(err.messageFormatted.substring(err.messageFormatted.indexOf('>> ')))
			this.emit('end')
		},
		htmlmin (err) {
			console.log(`\n\u001b[31m${err.name}\u001b[0m in plugin \'\u001b[36m${err.plugin}\u001b[0m\'`)
			console.log(`${err.fileName.replace(/\\/g, '/')}: ${err.message.slice(0, 500).replace('Parse Error:', 'Invalid HTML:')}`)
			this.emit('end')
		}
	}
}

const $ = require('gulp-load-plugins')({
	scope: ['devDependencies'],
	rename: {
		'gulp-cached':					'cache',
		'gulp-angular-templatecache':	'ngTemplatecache',
		'gulp-angular-filesort':		'ngFilesort'
	}
})

const getTemplates = () => {
	return gulp.src(config.template.source)
		.pipe($.cache('templateOut'))
			.pipe($.cache('template'))
				.pipe($.sourcemaps.init())
				.pipe($.htmlmin(config.htmlmin))
					.on('error', config.errorHandler.htmlmin)
			.pipe($.remember('template'))
			.pipe($.ngTemplatecache(config.js.filename, config.ngTemplatecache))
		.pipe($.remember('templateOut'))
}

gulp.task('index', () => {
	let index		= config.index
	let htmlFilter	= $.filter('**/*.html', { restore: true })

	return gulp.src(index.source)
		.pipe($.useref({}, lazy().pipe($.sourcemaps.init, { loadMaps: true })))
		.pipe(htmlFilter)
			.pipe($.htmlmin(config.htmlmin))
				.on('error', config.errorHandler.htmlmin)
		.pipe(htmlFilter.restore)
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(index.destination))
})

gulp.task('js', () => {
	let js = config.js

	return gulp.src(js.source)
		.pipe($.cache('js'))
			.pipe($.sourcemaps.init())
			.pipe($.eslint(config.eslint))
			.pipe($.eslint.format())
			.pipe($.babel(config.babel))
				.on('error', config.errorHandler.babel)
			.pipe($.ngAnnotate())
			.pipe($.uglify(config.uglify))
		.pipe($.remember('js'))
		.pipe($.ngFilesort())
		.pipe(addStream.obj(getTemplates()))
		.pipe($.concat(js.filename))
		.pipe($.header(js.header))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(js.destination))
})

gulp.task('css', () => {
	let css				= config.css
	let importsFilter	= $.filter('**/_*.scss', { restore: true })

	return gulp.src(css.source)
		// Only cache imported SCSS
		.pipe(importsFilter)
			.pipe($.cache('css'))
		.pipe(importsFilter.restore)
				.pipe($.sourcemaps.init())
				.pipe($.sass(config.sass))
					.on('error', config.errorHandler.sass)
				.pipe($.autoprefixer(config.autoprefixer))
				.pipe($.csso(config.csso))
		.pipe($.remember('css'))
		.pipe($.concat(css.filename))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest(css.destination))
		.pipe(browserSync.stream({ match: '**/*.css' }))
})

gulp.task('image', () => {
	let image		= config.image
	let destination	= image.destination

	return gulp.src(image.source)
		.pipe($.changed(destination))
		.pipe($.imagemin())
		.pipe(gulp.dest(destination))
})

gulp.task('font', () => {
	let font		= config.font
	let destination	= font.destination

	return gulp.src(font.source)
		.pipe($.rename(path => path.dirname = ''))
		.pipe($.changed(destination))
		.pipe(gulp.dest(destination))
})

gulp.task('dependencies', () => {
	return gulp.src(config.dependencies)
		.pipe($.install({ allowRoot: true }))
})

gulp.task('serve:index', ['index'], () => browserSync.reload())

gulp.task('serve:js', ['js'], () => browserSync.reload())

gulp.task('watch:index', () => $.watch(config.index.source, () => gulp.start('serve:index')))

gulp.task('watch:js', () => {
	$.watch(config.js.source, () => gulp.start('serve:js'))
		.on('unlink', path => {
			delete $.cache.caches['js'][path]
			$.remember.forget('js', path)
		})
})

gulp.task('watch:template', () => {
	$.watch(config.template.source, () => gulp.start('serve:js'))
		.on('unlink', path => {
			delete $.cache.caches['template'][path]
			delete $.cache.caches['templateOut']
			$.remember.forget('template', path)
			$.remember.forgetAll('templateOut')
		})
		.on('unlinkDir', path => $.cache.caches['template'] = [])
})

gulp.task('watch:css', () => {
	$.watch(config.css.source, () => gulp.start('css'))
		.on('unlink', path => {
			$.remember.forget('css', path.replace(/\.scss$/, '.css'))
			delete $.cache.caches['css'][path]
		})
		.on('unlinkDir', path => $.cache.caches['css'] = [])
})

gulp.task('watch:image', () => $.watch(config.image.source, () => gulp.start('image')))

gulp.task('watch:font', () => $.watch(config.font.source, () => gulp.start('font')))

gulp.task('build', ['dependencies'], () => gulp.start('index', 'js', 'css', 'image', 'font'))

const watch = () => gulp.start('watch:index', 'watch:js', 'watch:template', 'watch:css', 'watch:image', 'watch:font')

gulp.task('watch', ['build'], watch)

gulp.task('serve', ['build'], () => {
	let browserSyncConfig = config.browserSync
	browserSyncConfig.server.middleware = [
		proxy('/api', { target: 'http://localhost:8080' }),
		proxy('/images/project', { target: 'http://localhost:8080' }),
		proxy('/images/prototype', { target: 'http://localhost:8080' })
	]
	browserSync.init(browserSyncConfig)
	watch()
})

gulp.task('default', ['serve'])
